import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../core/core.dart';

class CommonButton extends StatelessWidget {
  const CommonButton(
      {Key? key,
      this.color,
      required this.onPressed,
      this.label = "",
      this.isImageLabel = false,
      this.imageLabel,
      this.iconLabel,
      this.padding,
      this.borderRadius,
      this.isLoading = false,
      this.borderColor,
      this.bordered = false,
      this.disable = false,
      this.fontLabelSize = 0,
      this.iconFirst = false,
      this.isIconLabel = false,
      this.labelColor,
      this.minHeight = 0,
      this.paddingIconLabel = 0})
      : super(key: key);

  final bool isLoading;
  final void Function()? onPressed;
  final Color? color;
  final Color? labelColor;
  final bool bordered;
  final Color? borderColor;
  final EdgeInsetsGeometry? padding;
  final String label;
  final bool isImageLabel;
  final Image? imageLabel;
  final Icon? iconLabel;
  final bool isIconLabel;
  final double fontLabelSize;
  final double minHeight;
  final double paddingIconLabel;
  final BorderRadius? borderRadius;
  final bool disable;
  final bool iconFirst;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: padding ??
          const EdgeInsets.symmetric(vertical: 15.0, horizontal: 30.0),
      child: ConstrainedBox(
        constraints: BoxConstraints(
          minWidth: double.infinity,
          minHeight: minHeight == 0 ? 50.0 : minHeight,
        ),
        child: InkWell(
          // highlightColor: Palette.buttonGreen2,
          onTap: isLoading || disable ? null : onPressed,
          child: Container(
            decoration: BoxDecoration(
              borderRadius: borderRadius ?? BorderRadius.circular(24.0),
              border: Border.fromBorderSide(BorderSide(
                  color: bordered == true
                      ? (borderColor ?? Get.theme.primaryColor)
                      : Colors.transparent,
                  width: 1.0)),
              color: bordered == true
                  ? Colors.transparent
                  : disable
                      ? Palette.hexToColor("#F4F4F4")
                      : color ?? Palette.kPrimary,
            ),
            child: isLoading
                ? SizedBox(
                    width: 25.0,
                    height: 25.0,
                    child: Center(
                      child: CircularProgressIndicator(
                        strokeWidth: 2.0,
                        valueColor: AlwaysStoppedAnimation<Color>(
                            Get.theme.primaryColor),
                      ),
                    ))
                : iconFirst
                    ? Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Padding(
                            padding: EdgeInsets.only(right: paddingIconLabel),
                            child: Visibility(
                              visible: isImageLabel,
                              child: imageLabel ?? Container(),
                            ),
                          ),
                          Padding(
                            padding: EdgeInsets.only(right: paddingIconLabel),
                            child: Visibility(
                              visible: isIconLabel,
                              child: iconLabel ?? Container(),
                            ),
                          ),
                          Text(
                            label,
                            style: TextStyle(
                                color: disable == true
                                    ? Palette.kTextGrey
                                    : labelColor ?? Colors.black,
                                fontSize:
                                    fontLabelSize == 0 ? 14.0 : fontLabelSize,
                                fontWeight: FontWeight.w600),
                          ),
                        ],
                      )
                    : Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Text(
                            label,
                            style: TextStyle(
                                color: disable == true
                                    ? Palette.kButtonDisable
                                    : labelColor ?? Colors.black,
                                fontSize:
                                    fontLabelSize == 0 ? 14.0 : fontLabelSize,
                                fontWeight: FontWeight.w600),
                          ),
                          Padding(
                            padding: EdgeInsets.only(left: paddingIconLabel),
                            child: Visibility(
                              visible: isImageLabel,
                              child: imageLabel ?? Container(),
                            ),
                          ),
                          Padding(
                            padding: EdgeInsets.only(left: paddingIconLabel),
                            child: Visibility(
                              visible: isIconLabel,
                              child: iconLabel ?? Container(),
                            ),
                          ),
                        ],
                      ),
          ),
        ),
      ),
    );
  }
}
