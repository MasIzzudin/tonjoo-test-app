import 'package:path/path.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:sqflite/sqflite.dart';
import 'package:tonjoo_test_app/app/modules/home_page/models/users.dart';

class DBProvider {
  DBProvider._();

  static final DBProvider db = DBProvider._();
  Database? _database;

  // ===== START INIT DB =====//

  Future<Database> get database async {
    if (_database != null) return _database!;

    var db = await initDB();
    _database = db;
    return db;
  }

  Future initDB() async {
    var databasePath = await getDatabasesPath();
    String path = join(databasePath, "tonjoo.db");

    return await openDatabase(
      path,
      version: 1,
      onOpen: (db) {},
      onCreate: onCreateTable,
    );
  }

  void onCreateTable(Database db, int version) async {
    db.execute("CREATE TABLE users("
        "id TEXT PRIMARY KEY,"
        "username TEXT,"
        "last_name TEXT,"
        "email TEXT,"
        "gender TEXT,"
        "avatar TEXT"
        ")");
  }

  // ======= END INIT DB ======== //

  // ====== START CRUD ====== //

  addUser(User user) async {
    try {
      final db = await database;
      Map allField = user.toJson();
      List fieldParam = [...allField.values];
      final placeholders = List.filled(fieldParam.length, '?').join(',');
      var raw = await db.rawInsert(
          "INSERT INTO users ("
          "id,"
          "username,"
          "last_name,"
          "email,"
          "gender,"
          "avatar)"
          " VALUES ($placeholders)",
          fieldParam);

      return raw;
    } catch (e) {
      rethrow;
    }
  }

  Future<List<User>> getListUser([String? search]) async {
    try {
      final db = await database;
      String extensionParam = '';
      if (search != null) {
        extensionParam =
            " WHERE username LIKE '%$search%' OR email LIKE '%$search%'";
      }

      String query = "SELECT * FROM users$extensionParam ORDER BY username ASC";
      print(query);
      var res = await db.rawQuery(query);
      return List.generate(res.length, (index) {
        Map<String, dynamic> newRes = {...res[index]};
        return User.fromJson(newRes);
      });
    } catch (e) {
      rethrow;
    }
  }

  deleteUserById(String id) async {
    try {
      final db = await database;

      var raw = await db.delete("users", where: 'id=?', whereArgs: [id]);
      return raw;
    } catch (e) {
      rethrow;
    }
  }

  deleteAll() async {
    final db = await database;
    SharedPreferences prefs = await SharedPreferences.getInstance();
    db.rawDelete("DELETE FROM users");
    prefs.clear();
  }
}
