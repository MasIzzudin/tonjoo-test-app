import 'package:flutter/material.dart';

class FontSize {
  static const double f4 = 4;
  static const double f8 = 8;
  static const double f10 = 10;
  static const double f12 = 12;
  static const double f14 = 14;
  static const double f18 = 18;
  static const double f30 = 30;
}

class Size {
  static const double p4 = 4;
  static const double p8 = 8;
  static const double p10 = 10;
  static const double p12 = 12;
  static const double p14 = 14;
  static const double p18 = 18;
  static const double p20 = 20;
  static const double p42 = 42;
  static const double p50 = 50;
  static const double p100 = 100;
}

const gapW0 = SizedBox(width: 0);
const gapW4 = SizedBox(width: 4);
const gapW8 = SizedBox(width: 8);
const gapW10 = SizedBox(width: 10);
const gapW12 = SizedBox(width: 12);
const gapW14 = SizedBox(width: 14);
const gapW16 = SizedBox(width: 16);
const gapW17 = SizedBox(width: 17);
const gapW20 = SizedBox(width: 20);
const gapW22 = SizedBox(width: 22);
const gapW24 = SizedBox(width: 24);
const gapW27 = SizedBox(width: 27);
const gapW32 = SizedBox(width: 32);
const gapW48 = SizedBox(width: 48);
const gapW64 = SizedBox(width: 64);

/// Constant gap heights
const gapH4 = SizedBox(height: 4);
const gapH6 = SizedBox(height: 6);
const gapH8 = SizedBox(height: 8);
const gapH10 = SizedBox(height: 10);
const gapH11 = SizedBox(height: 11);
const gapH12 = SizedBox(height: 12);
const gapH16 = SizedBox(height: 16);
const gapH20 = SizedBox(height: 20);
const gapH22 = SizedBox(height: 22);
const gapH24 = SizedBox(height: 24);
const gapH28 = SizedBox(height: 28);
const gapH32 = SizedBox(height: 32);
const gapH40 = SizedBox(height: 40);
const gapH44 = SizedBox(height: 44);
const gapH48 = SizedBox(height: 48);
const gapH54 = SizedBox(height: 54);
const gapH64 = SizedBox(height: 64);
const gapH72 = SizedBox(height: 72);
