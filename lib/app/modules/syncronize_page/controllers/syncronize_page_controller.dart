import 'package:get/get.dart';
import 'package:tonjoo_test_app/app/modules/home_page/models/users.dart';
import 'package:tonjoo_test_app/app/modules/syncronize_page/services/sync_services.dart';
import 'package:tonjoo_test_app/app/routes/app_pages.dart';
import 'package:tonjoo_test_app/core/database/db_provider.dart';

class SyncronizePageController extends GetxController {
  //TODO: Implement SyncronizePageController

  SyncServices ss = SyncServices();

  @override
  void onReady() {
    // TODO: implement onReady
    super.onReady();

    getUser();
  }

  getUser() async {
    List response = await ss.getUser();

    for (var element in response) {
      User data = User.fromJson(element);
      await DBProvider.db.addUser(data);
    }

    Get.offAllNamed(Routes.homePage);
  }
}
