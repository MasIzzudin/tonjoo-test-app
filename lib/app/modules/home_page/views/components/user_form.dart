import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:tonjoo_test_app/app/modules/home_page/controllers/home_page_controller.dart';
import 'package:tonjoo_test_app/app/widgets/widgets.dart';
import 'package:tonjoo_test_app/core/utils/utils.dart';

class UserForm extends GetView<HomePageController> {
  const UserForm({super.key});

  @override
  Widget build(BuildContext context) {
    return GetBuilder<HomePageController>(
      builder: (_) {
        return Padding(
          padding: const EdgeInsets.all(16),
          child: Form(
            key: _.formKey,
              child: SingleChildScrollView(
                child: Column(
                          children: [
                TextForm(
                  controller: _.nameController,
                  hintText: "Name",
                  validator: (value) {
                    if (value == "") {
                      return "Tidak boleh kosong";
                    }
                    return null;
                  },
                  prefixIcon: const Icon(Icons.person),
                ),
                gapH12,
                TextForm(
                  controller: _.lastNameController,
                  hintText: "Last Name",
                  validator: (value) {
                    if (value == "") {
                      return "Tidak boleh kosong";
                    }
                    return null;
                  },
                  prefixIcon: const Icon(Icons.person),
                ),
                gapH12,
                TextForm(
                  controller: _.genderController,
                  hintText: "Gender",
                  validator: (value) {
                    if (value == "") {
                      return "Tidak boleh kosong";
                    }
                    return null;
                  },
                ),
                gapH12,
                TextForm(
                  controller: _.emailController,
                  textInputType: TextInputType.emailAddress,
                  hintText: "Email",
                  validator: (value) {
                    if (value == "") {
                      return "Tidak boleh kosong";
                    }
                    return null;
                  },
                  prefixIcon: const Icon(Icons.email),
                ),
                gapH20,
                CommonButton(
                  padding: EdgeInsets.zero,
                  labelColor: Colors.white,
                  label: "Save",
                  onPressed: () => _.saveForm()
                )
              ],
            ),
          )),
        );
      },
    );
  }
}
