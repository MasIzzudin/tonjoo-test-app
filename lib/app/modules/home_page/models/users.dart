// To parse this JSON data, do
//
//     final user = userFromJson(jsonString);

import 'dart:convert';

class User {
    final String? id;
    final String? username;
    final String? lastName;
    final String? email;
    final String? gender;
    final String? avatar;

    User({
        this.id,
        this.username,
        this.lastName,
        this.email,
        this.gender,
        this.avatar,
    });

    factory User.fromRawJson(String str) => User.fromJson(json.decode(str));

    String toRawJson() => json.encode(toJson());

    factory User.fromJson(Map<String, dynamic> json) => User(
        id: json["id"] ?? "",
        username: json["username"] ?? "",
        lastName: json["last_name"] ?? "",
        email: json["email"] ?? "",
        gender: json["gender"] ?? "",
        avatar: json["avatar"] ?? "",
    );

    Map<String, dynamic> toJson() => {
        "id": id ?? "",
        "username": username ?? "",
        "last_name": lastName ?? "",
        "email": email ?? "",
        "gender": gender ?? "",
        "avatar": avatar ?? "",
    };
}
