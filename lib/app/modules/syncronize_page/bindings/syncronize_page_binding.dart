import 'package:get/get.dart';

import '../controllers/syncronize_page_controller.dart';

class SyncronizePageBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<SyncronizePageController>(
      () => SyncronizePageController(),
    );
  }
}
