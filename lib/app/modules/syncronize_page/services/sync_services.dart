import 'package:dio/dio.dart';
import 'package:tonjoo_test_app/core/core.dart';

class SyncServices {
  DioService dioService = DioService();
  Response? response;

  Future<List> getUser() async {
    try {
      String path = "users";
      response = await dioService.dio.get(path);

      return response!.data as List;
    } on DioException {
      rethrow;
    }
  }
}
