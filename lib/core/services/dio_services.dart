import 'package:dio/dio.dart';
import 'package:tonjoo_test_app/core/utils/constant.dart';

import 'app_interceptors.dart';
import 'logger_interceptor.dart';

class DioService {
  final String? token;
  Dio dio = Dio(
    BaseOptions(
      connectTimeout: const Duration(milliseconds: 30000),
      receiveTimeout: const Duration(milliseconds: 30000),
      baseUrl: baseUrl,
    ),
  );
  DioService({this.token}) {
    dio.interceptors.addAll([
      AppInterceptor(), LoggerInterceptor()
    ]);
  }
}
