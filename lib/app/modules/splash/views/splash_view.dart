import 'package:flutter/material.dart';

import 'package:get/get.dart';
import 'package:tonjoo_test_app/core/utils/app_assets.dart';

import '../controllers/splash_controller.dart';

class SplashView extends GetView<SplashController> {
  const SplashView({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: GetBuilder<SplashController>(
        builder: (context) {
          return SizedBox(
            height: Get.height,
            width: Get.width,
            child: Center(
              child: Image.asset(AppAsset.tonjooIcon)
            ),
          );
        }
      ),
    );
  }
}
