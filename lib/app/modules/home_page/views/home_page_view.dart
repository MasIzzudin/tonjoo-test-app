import 'package:flutter/material.dart';

import 'package:get/get.dart';
import 'package:tonjoo_test_app/core/utils/utils.dart';

import '../controllers/home_page_controller.dart';

class HomePageView extends GetView<HomePageController> {
  const HomePageView({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return MixinBuilder<HomePageController>(
      builder: (_) {
        return Scaffold(
          appBar: AppBar(
            backgroundColor: Palette.kPrimary,
            title: Text(_.titlePage.value),
            centerTitle: true,
          ),
          body: _.pages[_.initialPage],
          bottomNavigationBar: BottomNavigationBar(
            type: BottomNavigationBarType.fixed,
            fixedColor: Palette.kPrimary,
            onTap: (value) => _.setPage(value),
            currentIndex: _.initialPage,
            items: const [
              BottomNavigationBarItem(
                  icon: Icon(Icons.home),
                  label: "Home"),
              BottomNavigationBarItem(
                  icon: Icon(Icons.add_box_rounded),
                  label: "Add New"),
              BottomNavigationBarItem(
                  icon: Icon(Icons.logout),
                  label: "Logout"),
            ],
          ),
        );
      }
    );
  }
}
