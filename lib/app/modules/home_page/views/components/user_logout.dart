import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:tonjoo_test_app/app/modules/home_page/controllers/home_page_controller.dart';
import 'package:tonjoo_test_app/app/widgets/widgets.dart';

class UserLogout extends GetView<HomePageController> {
  const UserLogout({super.key});

  @override
  Widget build(BuildContext context) {
    return Center(
      child: CommonButton(
        onPressed: () => controller.logout(),
        label: "Keluar",
        labelColor: Colors.white,
      )
    );
  }
}