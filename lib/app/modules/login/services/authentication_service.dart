import 'package:dio/dio.dart';
import 'package:tonjoo_test_app/core/core.dart';

class AuthenticationService {
  DioService dioService = DioService();
  Response? response;

  Future<Map> loginRequest(
      {required username, required String password}) async {
    try {
      String path = "login";
      response = await dioService.dio
          .post(path, data: {"user": username, "password": password});

      return response!.data as Map;
    } on DioException {
      rethrow;
    }
  }
}
