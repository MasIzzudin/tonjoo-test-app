import 'package:get/get.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:tonjoo_test_app/app/routes/app_pages.dart';

import '../../../../core/core.dart';

class SplashController extends GetxController {
  //TODO: Implement SplashController

  @override
  void onInit() {
    super.onInit();

    Future.delayed(const Duration(seconds: 3), () => getPref());
  }

  getPref() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String? token = prefs.getString(KeyPrefs.token);

    if (token == null) {
      Get.offAllNamed(Routes.loginPage);
    } else {
      Get.offAllNamed(Routes.homePage);
    }
  }
}
