import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:get/get.dart';
import 'package:tonjoo_test_app/app/modules/home_page/controllers/home_page_controller.dart';
import 'package:tonjoo_test_app/app/modules/home_page/models/users.dart';
import 'package:tonjoo_test_app/app/widgets/widgets.dart';
import 'package:tonjoo_test_app/core/utils/utils.dart';

class UserPage extends GetView<HomePageController> {
  const UserPage({super.key});

  @override
  Widget build(BuildContext context) {
    return GetBuilder<HomePageController>(
      init: HomePageController(),
      initState: (_) {
        controller.initUserPage();
        controller.getUser();
      },
      builder: (_) {
        return ConditionalWidget(
          condition: _.isLoadingGet.value,
          builder: (context) => Center(
            child: CircularProgressIndicator(
              color: Palette.kPrimary,
            ),
          ),
          fallback: (context) => Padding(
            padding: const EdgeInsets.all(16.0),
            child: Column(
              children: [
                Container(
                  decoration: BoxDecoration(
                    color: Colors.white,
                    border: Border.all(
                      color: Colors.grey
                    ),
                    borderRadius: BorderRadius.circular(10)
                  ),
                  child: TextForm(
                    controller: _.searchController,
                    hintText: "Cari...",
                    sufixIcon: const Icon(Icons.search),
                    onChanged: (value) => _.getUser(value),
                  ),
                ),
                gapH32,
                Expanded(
                  child: Column(
                    children: [
                      Expanded(
                        child: ListView.builder(
                          controller: _.scrollController,
                          shrinkWrap: false,
                          itemCount: _.userList.length,
                          itemBuilder: (context, index) {
                            User data = _.userList[index];
                            return Container(
                              margin: const EdgeInsets.symmetric(vertical: 10),
                              child: ListTile(
                                contentPadding: EdgeInsets.zero,
                                leading: Container(
                                  height: 50,
                                  width: 50,
                                  decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(100)
                                  ),
                                  child: ClipRRect(
                                    borderRadius: BorderRadius.circular(100),
                                    child: data.avatar!.isNotEmpty ? Image.network(data.avatar!) : const Icon(Icons.person)),
                                ),
                                title: CommonText(text: data.username, size: FontSize.f18,),
                                subtitle: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    CommonText(text: data.gender, size: FontSize.f12,),
                                    gapH8,
                                    CommonText(text: data.email, size: FontSize.f14,),
                                  ],
                                ),
                                trailing: InkWell(
                                  onTap: () => _.deleteUser(data.id!),
                                  child: const Icon(Icons.close),
                                ),
                              ),
                            );
                          },
                        ),
                      ),
                      Obx(() => ConditionalWidget(
                        condition: _.isScrolling.value == true, 
                        builder: (context) => ConditionalWidget(
                          condition: _.isOnline.value == true,
                          builder: (context) => SizedBox(
                            height: Size.p20,
                            width: Get.width,
                            child: Center(child: SpinKitThreeBounce(
                              color: Palette.kPrimary,
                              size: 20,
                            ))
                          ),
                          fallback: (context) => Container(
                            margin: const EdgeInsets.symmetric(vertical: Size.p20),
                            child: CommonText(
                              text: "Anda sedang offline",
                              color: Colors.red,
                              weight: FontWeight.w800,
                              size: 16,
                            ),
                          ),
                        ),
                      ))
                    ],
                  ),
                ),
              ],
            ),
          ),
        );
      },
    );
  }
}
