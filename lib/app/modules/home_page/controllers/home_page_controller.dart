import 'dart:async';

import 'package:connectivity_plus/connectivity_plus.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:tonjoo_test_app/app/modules/home_page/models/users.dart';
import 'package:tonjoo_test_app/app/modules/home_page/views/components/user_form.dart';
import 'package:tonjoo_test_app/app/modules/home_page/views/components/user_logout.dart';
import 'package:tonjoo_test_app/app/modules/home_page/views/components/user_page.dart';
import 'package:tonjoo_test_app/app/modules/syncronize_page/services/sync_services.dart';
import 'package:tonjoo_test_app/app/routes/app_pages.dart';
import 'package:tonjoo_test_app/app/widgets/widgets.dart';
import 'package:tonjoo_test_app/core/database/db_provider.dart';
import 'package:uuid/uuid.dart';

class HomePageController extends GetxController {
  //TODO: Implement HomePageController

  SyncServices ss = SyncServices();

  TextEditingController searchController = TextEditingController();
  TextEditingController nameController = TextEditingController();
  TextEditingController lastNameController = TextEditingController();
  TextEditingController genderController = TextEditingController();
  TextEditingController emailController = TextEditingController();
  Connectivity connectivity = Connectivity();
  late StreamSubscription<ConnectivityResult> connectivitySubscription;

  late ScrollController scrollController;

  GlobalKey<FormState> formKey = GlobalKey<FormState>();

  List<Widget> pages = [const UserPage(), const UserForm(), const UserLogout()];
  int initialPage = 0;
  var isLoadingGet = false.obs;
  var titlePage = "Home".obs;
  List<User> userList = [];
  var isScrolling = false.obs;
  var isOnline = true.obs;

  initUserPage() async {
    scrollController = ScrollController();
    scrollController.addListener(_onScrollListener);
    connectivitySubscription =
        connectivity.onConnectivityChanged.listen((ConnectivityResult result) {
      if (result == ConnectivityResult.none) {
        isOnline.value = false;
      } else {
        isOnline.value = true;
      }
    });
  }

  void _onScrollListener() {
    if (scrollController.position.pixels ==
        scrollController.position.maxScrollExtent) {
      isScrolling.value = true;
      if (isOnline.value == true) {
        loadMoreUser();
      }
    } else {
      isScrolling.value = false;
    }
  }

  setPage(int index) {
    initialPage = index;

    if (index == 1) {
      titlePage.value = "Create User";
    } else if (index == 2) {
      titlePage.value = "Keluar";
    } else {
      titlePage.value = "Home";
    }

    update();
  }

  getUser([String? search]) async {
    try {
      isLoadingGet.value = true;
      userList = await DBProvider.db.getListUser(search);
      isLoadingGet.value = false;
    } catch (e) {
      isLoadingGet.value = false;
      rethrow;
    }

    update();
  }

  loadMoreUser() async {
    try {
      List response = await ss.getUser();
      for (var element in response) {
        User data = User.fromJson(element);
        await DBProvider.db.addUser(data);
        userList.add(data);
      }

      isScrolling.value = false;

      update();
    } catch (e) {
      isScrolling.value = false;
      rethrow;
    }
  }

  saveForm() async {
    if (formKey.currentState!.validate()) {
      formKey.currentState!.save();

      String name = nameController.text;
      String lastName = lastNameController.text;
      String gender = genderController.text;
      String email = emailController.text;

      User data = User(
          id: const Uuid().v1(),
          username: name,
          lastName: lastName,
          gender: gender,
          email: email,
          avatar: "");

      try {
        await DBProvider.db.addUser(data);
        clearForm();
        Snackbar.success("Berhasil menyimpan user");
      } catch (e) {
        rethrow;
      }
    }
  }

  deleteUser(String id) async {
    try {
      await DBProvider.db.deleteUserById(id);
      userList.removeWhere((element) => element.id == id);
    } catch (e) {
      rethrow;
    }

    update();
  }

  clearForm() {
    nameController.clear();
    lastNameController.clear();
    genderController.clear();
    emailController.clear();
  }

  logout() async {
    await DBProvider.db.deleteAll();
    Get.offAllNamed(Routes.loginPage);
  }
}
