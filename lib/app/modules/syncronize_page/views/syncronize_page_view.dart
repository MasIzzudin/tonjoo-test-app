import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';

import 'package:get/get.dart';
import 'package:tonjoo_test_app/app/widgets/widgets.dart';
import 'package:tonjoo_test_app/core/core.dart';

import '../controllers/syncronize_page_controller.dart';

class SyncronizePageView extends GetView<SyncronizePageController> {
  const SyncronizePageView({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Scaffold(body: GetBuilder<SyncronizePageController>(
      builder: (_) {
        return Container(
          padding: const EdgeInsets.symmetric(horizontal: Size.p18),
          width: Get.width,
          height: Get.height,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              SpinKitFadingCube(color: Palette.kPrimary),
              gapH32,
              CommonText(
                text: "Mohon menunggu, proses sinkronisasi sedang berjalan.",
                align: TextAlign.center,
              )
            ],
          ),
        );
      },
    ));
  }
}
