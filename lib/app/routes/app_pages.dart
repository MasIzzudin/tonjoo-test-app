import 'package:get/get.dart';

import '../modules/home_page/bindings/home_page_binding.dart';
import '../modules/home_page/views/home_page_view.dart';
import '../modules/login/bindings/login_binding.dart';
import '../modules/login/views/login_view.dart';
import '../modules/splash/bindings/splash_binding.dart';
import '../modules/splash/views/splash_view.dart';
import '../modules/syncronize_page/bindings/syncronize_page_binding.dart';
import '../modules/syncronize_page/views/syncronize_page_view.dart';

part 'app_routes.dart';

class AppPages {
  AppPages._();

  static const initial = Routes.splashPage;

  static final routes = [
    GetPage(
      name: _Paths.splashPage,
      page: () => const SplashView(),
      binding: SplashBinding(),
    ),
    GetPage(
      name: _Paths.loginPage,
      page: () => const LoginView(),
      binding: LoginBinding(),
    ),
    GetPage(
      name: _Paths.syncronizePage,
      page: () => const SyncronizePageView(),
      binding: SyncronizePageBinding(),
    ),
    GetPage(
      name: _Paths.homePage,
      page: () => const HomePageView(),
      binding: HomePageBinding(),
    ),
  ];
}
