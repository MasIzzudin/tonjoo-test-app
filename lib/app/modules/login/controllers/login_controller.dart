import 'dart:async';

import 'package:connectivity_plus/connectivity_plus.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:tonjoo_test_app/app/modules/login/services/authentication_service.dart';
import 'package:tonjoo_test_app/app/routes/app_pages.dart';

import '../../../../core/core.dart';
import '../../../widgets/widgets.dart';

class LoginController extends GetxController {
  //TODO: Implement LoginController

  AuthenticationService acs = AuthenticationService();

  late TextEditingController usernameController;
  late TextEditingController passwordController;
  Connectivity connectivity = Connectivity();
  late StreamSubscription<ConnectivityResult> connectivitySubscription;
  late SharedPreferences prefs;

  var showPassword = false.obs;
  var disableButton = false.obs;
  var loadingButton = false.obs;

  @override
  void onInit() {
    // TODO: implement onInit
    super.onInit();

    usernameController = TextEditingController();
    passwordController = TextEditingController();
    connectivitySubscription =
        connectivity.onConnectivityChanged.listen((ConnectivityResult result) {
      if (result == ConnectivityResult.none) {
        disableButton.value = true;
      } else {
        disableButton.value = false;
      }
    });

    initPrefs();
  }

  initPrefs() async {
    prefs = await SharedPreferences.getInstance();
  }

  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
    connectivitySubscription.cancel();
  }

  setShowPassword(bool value) => showPassword.value = value;

  loginRequest() async {
    loadingButton.value = true;

    try {
      String user = usernameController.text;
      String pass = passwordController.text;
      var callBack = await acs.loginRequest(username: user, password: pass);
      prefs.setString(KeyPrefs.token, callBack["token"]);
      loadingButton.value = false;
      Get.offAllNamed(Routes.syncronizePage);
    } catch (e) {
      loadingButton.value = false;
      Snackbar.warning("Username atau password salah!");
      rethrow;
    }
  }
}
