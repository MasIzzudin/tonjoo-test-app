

import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../core/core.dart';
import 'conditional_widget.dart';

class Snackbar {
  static warning(String message, {
    Color? mainColor, 
    Color? textColor, 
    double? textSize,
    Duration? duration, 
    Widget? preffixWidget,
    ShapeBorder? shape,
  }) {
    ScaffoldMessenger.of(Get.context!).showSnackBar(
      SnackBar(
        width: 250,
        elevation: 0,
        shape: shape ?? RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(0)
        ),
        dismissDirection: DismissDirection.down,
        behavior: SnackBarBehavior.floating,
        content: ConditionalWidget(
          condition: preffixWidget != null, 
          builder: (ctx) => Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              preffixWidget!,
              const SizedBox(width: 20,),
              Text(
                message,
                style: TextStyle(
                  color: textColor ?? Colors.white,
                  fontWeight: FontWeight.bold,
                  fontSize: 14,
                ),
              )
            ],
          ),
          fallback: (ctx) => Text(
            message,
            style: TextStyle(
              color: textColor ?? Colors.white,
              fontWeight: FontWeight.bold,
              fontSize: 14,
            ),
          ),
        ),
        duration: duration ?? const Duration(seconds: 3),
        backgroundColor: mainColor ?? Palette.alertWarning,
      ),
    );
  }

  static success(String message, {
    Color? mainColor, 
    Color? textColor, 
    double? textSize,
    Duration? duration, 
    Widget? preffixWidget,
    ShapeBorder? shape,
    Widget? suffixWidget
  }) {
    ScaffoldMessenger.of(Get.context!).showSnackBar(
      SnackBar(
        elevation: 0,
        shape: shape ?? RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(0)
        ),
        dismissDirection: DismissDirection.down,
        behavior: SnackBarBehavior.floating,
        content: Row(
          children: [
            ConditionalWidget(
              condition: preffixWidget != null, 
              builder: (ctx) => preffixWidget!,
              fallback: (context) => Container(),
            ),
            const SizedBox(width: 20,),
            Expanded(
              child: Text(
                message,
                style: TextStyle(
                  color: textColor ?? Colors.white,
                  fontWeight: FontWeight.bold,
                  fontSize: textSize ?? 14,
                ),
              ),
            ),
            const SizedBox(width: 8,),
            ConditionalWidget(
              condition: suffixWidget != null, 
              builder: (ctx) => suffixWidget!,
              fallback: (context) => Container(),
            ),
          ],
        ),
        duration: duration ?? const Duration(seconds: 3),
        backgroundColor: mainColor ?? Palette.alertSuccess,
      ),
    );
  }
}