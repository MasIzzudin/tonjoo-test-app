import 'package:flutter/material.dart';

class Palette {
  static Color hexToColor(String code) {
    return Color(int.parse(code.substring(1, 7), radix: 16) + 0xFF000000);
  }

  static String colorToHex(Color color) {
    var colorString = color.toString();
    var convertColorToHex =
        "#${colorString.substring(10, colorString.length - 1)}";
    return convertColorToHex;
  }

  static Color kPrimary = Palette.hexToColor("#f05738");
  static Color kButton = Palette.hexToColor("#f05738");
  static Color kButtonDisable = Palette.hexToColor("#9B9891");
  static Color kTextNormal = Palette.hexToColor("#251A04");
  static Color kTextGrey = Palette.hexToColor("#757575");
  static Color kTextWhite = Colors.white;
  static Color hintText = Palette.hexToColor("#B9B9B9");
  static Color borderInput = Palette.hexToColor("#F8F8F8");
  static Color errorBorderInput = Palette.hexToColor("#E82320");
  static Color alertWarning = Palette.hexToColor("#F7B731");
  static Color alertSuccess = Palette.hexToColor("#20BF6B");
}
