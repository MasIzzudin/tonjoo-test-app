part of 'app_pages.dart';
// DO NOT EDIT. This is code generated via package:get_cli/get_cli.dart

abstract class Routes {
  Routes._();
  static const loginPage = _Paths.loginPage;
  static const splashPage = _Paths.splashPage;
  static const syncronizePage = _Paths.syncronizePage;
  static const homePage = _Paths.homePage;
}

abstract class _Paths {
  _Paths._();
  static const loginPage = '/login-page';
  static const splashPage = '/splash-page';
  static const syncronizePage = '/syncronize-page';
  static const homePage = '/home-page';
}
