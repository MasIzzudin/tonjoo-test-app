import 'package:flutter/material.dart';

import 'package:get/get.dart';
import 'package:tonjoo_test_app/core/core.dart';
import 'package:tonjoo_test_app/core/utils/app_assets.dart';

import '../../../widgets/widgets.dart';
import '../controllers/login_controller.dart';

class LoginView extends GetView<LoginController> {
  const LoginView({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: Container(
          padding: const EdgeInsets.symmetric(horizontal: Size.p18, vertical: Size.p18),
          width: Get.width,
          child: GetBuilder<LoginController>(
            builder: (_) {
              return SingleChildScrollView(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    gapH72,
                    Image.asset(AppAsset.tonjooIcon, width: Size.p100, height: Size.p100,),
                    gapH72,
                    CommonText(text: "Login", 
                      color: Palette.kPrimary,
                      weight: FontWeight.w800,
                      size: FontSize.f30,
                    ),
                    gapH10,
                    CommonText(
                      text: "Silahkan login gunakan username dan password",
                      size: FontSize.f12,
                      color: Palette.kTextGrey,
                    ),
                    gapH20,
                    Material(
                      elevation: 1.0,
                      shadowColor: Palette.kTextGrey,
                      borderRadius: const BorderRadius.only(
                        topLeft: Radius.circular(Size.p10),
                        topRight: Radius.circular(Size.p10)
                      ),
                      child: TextForm(
                        prefixIcon: Icon(Icons.person, color: Palette.kTextGrey,),
                        controller: _.usernameController,
                        hintText: "Username",
                      ),
                    ),
                    Material(
                      elevation: 1.0,
                      shadowColor: Palette.kTextGrey,
                      borderRadius: const BorderRadius.only(
                        bottomLeft: Radius.circular(Size.p10),
                        bottomRight: Radius.circular(Size.p10)
                      ),
                      child: Obx(() => TextForm(
                        prefixIcon: Icon(Icons.lock, color: Palette.kTextGrey,),
                        sufixIcon: InkWell(
                          onTap: () => _.setShowPassword(!_.showPassword.value),
                          child: Icon(_.showPassword.value ? Icons.visibility : Icons.visibility_off, color: Palette.kTextGrey,),
                        ),
                        obsecureText: !_.showPassword.value,
                        controller: _.passwordController,
                        hintText: "Password",
                      ),)
                    ),
                    gapH24,
                    Obx(() => CommonButton(
                      padding: EdgeInsets.zero,
                      borderRadius: BorderRadius.circular(Size.p10),
                      label: "Login",
                      isLoading: _.loadingButton.value,
                      labelColor: Palette.kTextWhite,
                      disable: _.disableButton.value,
                      onPressed: () => _.loginRequest(),
                    ))
                  ],
                ),
              );
            }
          ),
        ),
      )
    );
  }
}
